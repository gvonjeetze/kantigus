// production environment

import {AppEnvironment} from './app-environment';
import {environment as defaultEnvironment} from './environment';

export const environment: AppEnvironment = {
  cadBaseUrl: defaultEnvironment.cadBaseUrl,
  featureList: defaultEnvironment.featureList,
  production: true,
  dataServiceId: '73f76ad0-eb7d-4705-8027-95ea792575e6',
  dataServiceUrl: 'https://data-kantigus.igus.tools',
  koplaServicesUrl: 'https://cdn-services.igus.tools',
  shopApiBaseUrl: 'https://cdn-services.igus.tools/shop',
  ipServiceUrl: 'https://cdn-services.igus.tools/client-ip',
  logLevel: 'error',
  cS: 'b534e028-c728-4d78-bf2e-1c53d1fe0641'
};
