// cordova environment

import {CordovaFeatureList} from '../app/feature-map';
import {AppEnvironment} from './app-environment';
import {environment as integrationEnvironment} from './environment.integration';
import {environment as productionEnvironment} from './environment.production';

export const environment: AppEnvironment = {
  production: integrationEnvironment.production,
  logLevel: integrationEnvironment.logLevel,
  koplaServicesUrl: productionEnvironment.koplaServicesUrl,
  shopApiBaseUrl: productionEnvironment.shopApiBaseUrl,
  dataServiceId: productionEnvironment.dataServiceId,
  dataServiceUrl: productionEnvironment.dataServiceUrl,
  ipServiceUrl: productionEnvironment.ipServiceUrl,
  cS: productionEnvironment.cS,
  featureList: CordovaFeatureList,
  useHash: true
};
