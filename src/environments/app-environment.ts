import {Environment} from 'kopla_app/environment';

export interface AppEnvironment extends Environment {
  koplaServicesUrl: string;
  dataServiceId: string;
  dataServiceUrl: string;
}
