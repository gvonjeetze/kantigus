// integration environment

import {AppEnvironment} from './app-environment';
import {environment as productionEnvironment} from './environment.production';

export const environment: AppEnvironment = {
  cadBaseUrl: productionEnvironment.cadBaseUrl,
  production: productionEnvironment.production,
  logLevel: productionEnvironment.logLevel,
  featureList: productionEnvironment.featureList,
  dataServiceId: 'e2dc9c4a-ba6d-44e2-9820-562ce5adbf75',
  dataServiceUrl: 'https://data-ki-kopla-integration.igusdev.igus.de',
  koplaServicesUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de',
  shopApiBaseUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de/shop',
  ipServiceUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de/client-ip',
  cS: '1d2ee893-b818-4363-8f1c-c2fddfdc4af2'
};
