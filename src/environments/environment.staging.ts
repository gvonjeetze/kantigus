// staging environment

import {AppEnvironment} from './app-environment';
import {environment as productionEnvironment} from './environment.production';

export const environment: AppEnvironment = {
  cadBaseUrl: productionEnvironment.cadBaseUrl,
  production: productionEnvironment.production,
  logLevel: productionEnvironment.logLevel,
  featureList: productionEnvironment.featureList,
  dataServiceId: '912fd177-f026-4b2f-afd5-19240a58bfe8',
  dataServiceUrl: 'https://data-ki-kopla-staging.igusdev.igus.de',
  koplaServicesUrl: 'https://cdn-services-kopla-staging.igusdev.igus.de',
  shopApiBaseUrl: 'https://cdn-services-kopla-staging.igusdev.igus.de/shop',
  ipServiceUrl: 'https://cdn-services-kopla-staging.igusdev.igus.de/client-ip',
  cS: '07e0193e-6e1b-4d45-bd56-8c832e090296'
};
