// This is the default environment.

import {ApplicationFeatureList} from '../app/feature-map';
import {AppEnvironment} from './app-environment';

export const environment: AppEnvironment = {
  production: false,
  dataServiceId: '25b10704-437b-4544-bf34-4f09fce0145d',
  dataServiceUrl: 'https://data-ki-kopla-integration.igusdev.igus.de',
  shopApiBaseUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de/shop',
  koplaServicesUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de',
  ipServiceUrl: 'https://cdn-services-kopla-integration.igusdev.igus.de/client-ip',
  logLevel: 'debug',
  cS: '72e57772-8680-4648-9ead-144758bb567d',
  featureList: ApplicationFeatureList
};
