import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {bootloader} from '@angularclass/hmr';
import {environment} from 'environment';
import {CordovaService} from 'kopla_app/cordova';
import {fromEvent} from 'rxjs';
import {AppModule} from './app/app.module';
import './locales';

if (environment.production) {
  enableProdMode();
}

export function main(): Promise<any> {
  if (CordovaService.appRunsOnCordovaPlatform(window)) {
    fromEvent(document, 'deviceready', {capture: false}).subscribe(() => {
      return platformBrowserDynamic().bootstrapModule(AppModule);
    });
  } else {
    return platformBrowserDynamic().bootstrapModule(AppModule);
  }
}

bootloader(main);
