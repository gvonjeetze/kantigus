import {registerLocaleData} from '@angular/common';

import de from '@angular/common/locales/de';

[de].forEach(locale => registerLocaleData(locale));
