import {ChangeDetectionStrategy, Component, Input, ViewEncapsulation} from '@angular/core';
import {Gericht} from '../shared/gericht-card/gericht-card.component';

export interface Allergene {
  id: number;
  code: string;
  beschreibung: string;
}

export interface Naehrwert {
  id: number;
  beschreibung: string;
  value: number;
}

@Component({
  selector: 'ki-day-component',
  templateUrl: './day-overview.component.html',
  styleUrls: ['./day-overview.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DayOverviewComponent {
  @Input() gerichte: Gericht[] = [
    {id: 1, name: 'gericht1', teaser: 'ich bin lecker'},
    {id: 2, name: 'gericht2', teaser: 'ich bin lecker'},
    {id: 3, name: 'gericht3', teaser: 'ich bin lecker'},
    {id: 4, name: 'gericht4', teaser: 'ich bin lecker'}
  ];

  constructor() {}
}
