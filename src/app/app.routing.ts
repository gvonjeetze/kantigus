import {Routes} from '@angular/router';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {ErrorComponent} from './error/error.component';
import {TranslationResolver} from './shared/translation.resolver';

export const appRoutes: Routes = [
  {
    path: '',
    component: AppComponent,
    resolve: {translations: TranslationResolver},
    canActivateChild: [],
    children: [
      {
        path: 'home',
        component: HomeComponent,
        data: {trackingLabel: 'Step 1'}
      },
      {path: 'error', component: ErrorComponent},
      {path: '', redirectTo: 'home', pathMatch: 'full'},
      {path: '**', redirectTo: 'home'} // 404
    ]
  }
];
