import {ChangeDetectorRef, ElementRef} from '@angular/core';
import {inject, TestBed} from '@angular/core/testing';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Angulartics2} from 'angulartics2';
import {Angulartics2GoogleAnalytics} from 'angulartics2/ga';
import {ContactFormService} from 'kopla_app/contact-form';
import {EnvironmentService} from 'kopla_app/environment';
import {ErrorHandler} from 'kopla_app/error';
import {FileService} from 'kopla_app/file';
import {DomainResolver, LocaleResolver} from 'kopla_app/g11n';
import {LiabilityDisclaimerService} from 'kopla_app/liability-disclaimer';
import {Logger} from 'kopla_app/logger';
import {ConfigurationManagementService} from 'kopla_app/menu';
import {LazyTranslatedNotificationService, NotificationService} from 'kopla_app/notification';
import {ShopSessionHelperService} from 'kopla_app/shop';
import {of} from 'rxjs';
import {AppComponent} from './app.component';
import {TrackingProviderService} from './shared/tracking-provider.service';
import {TranslationResolver} from './shared/translation.resolver';

class Angulartics2Mock {}

class Angulartics2GoogleTagManagerMock {}

class TrackingProviderServiceMock {}

class FileServiceMock {}

class EnvironmentServiceMock {
  getRawEnvironmentConfig() {
    return {};
  }

  getLogLevel() {
    return 'debug';
  }
}

class TranslateServiceMock {}

class TranslationResolverMock {
  resolve() {}
}

class RouterMock {
  events = {
    subscribe: () => {}
  };
}

class NotificationServiceMock {}

class ElementRefMock {
  get nativeElement() {
    return {
      scrollIntoView: () => {}
    };
  }
}

class ChangeDetectorRefMock {
  detectChanges() {}
}

class LiabilityDisclaimerServiceMock {
  reset() {}
}

class ConfigurationManagementServiceMock {}

class ContactFormServiceMock {}

class FormBuilderMock {}

class ShopSessionHelperServiceMock {}

class ErrorHandlerMock {}

class LazyTranslatedNotificationServiceMock {
  showNotifications() {}
}

class LocaleResolverMock {
  getLocale() {
    return of({language: 'de', countryCode: 'DE'});
  }
}

class DomainResolverMock {
  getDomainForCustomLocale() {
    return of('test.de');
  }
}

describe('App: Kantigus', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        AppComponent,
        {provide: TranslateService, useClass: TranslateServiceMock},
        {provide: TranslationResolver, useClass: TranslationResolverMock},
        {provide: Router, useClass: RouterMock},
        {provide: NotificationService, useClass: NotificationServiceMock},
        {provide: ElementRef, useClass: ElementRefMock},
        {provide: ChangeDetectorRef, useClass: ChangeDetectorRefMock},
        {provide: Logger, useClass: Logger},
        {provide: EnvironmentService, useClass: EnvironmentServiceMock},
        {provide: FileService, useClass: FileServiceMock},
        {provide: LiabilityDisclaimerService, useClass: LiabilityDisclaimerServiceMock},
        {provide: ConfigurationManagementService, useClass: ConfigurationManagementServiceMock},
        {provide: ContactFormService, useClass: ContactFormServiceMock},
        {provide: FormBuilder, useClass: FormBuilderMock},
        {provide: Angulartics2GoogleAnalytics, useClass: Angulartics2GoogleTagManagerMock},
        {provide: ShopSessionHelperService, useClass: ShopSessionHelperServiceMock},
        {provide: TrackingProviderService, useClass: TrackingProviderServiceMock},
        {provide: Angulartics2, useClass: Angulartics2Mock},
        {provide: ErrorHandler, useClass: ErrorHandlerMock},
        {provide: LocaleResolver, useClass: LocaleResolverMock},
        {provide: DomainResolver, useClass: DomainResolverMock},
        {provide: LazyTranslatedNotificationService, useClass: LazyTranslatedNotificationServiceMock}
      ]
    });
  });

  it('should create the app', inject([AppComponent], (app: AppComponent) => {
    expect(app).toBeTruthy();
  }));
});
