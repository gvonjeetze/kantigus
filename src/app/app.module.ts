import {HttpClient, HttpClientModule} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {TranslateLoader as NgTranslateLoader, TranslateModule} from '@ngx-translate/core';
import {environment} from 'environment';
import {AboutModule} from 'kopla_app/about';
import {ButtonModule} from "kopla_app/button";
import {CardModule} from 'kopla_app/card';
import {CardWrapperModule} from 'kopla_app/card-wrapper';
import {CheckboxModule} from 'kopla_app/checkbox';
import {ContactFormModule} from 'kopla_app/contact-form';
import {CookiePolicyModule} from 'kopla_app/cookie-policy';
import {CordovaFileService, CordovaModule} from 'kopla_app/cordova';
import {DeviceInfoModule} from 'kopla_app/device-info';
import {DialogModule} from "kopla_app/dialog";
import {DropdownModule} from 'kopla_app/dropdown';
import {FeatureFlagModule} from 'kopla_app/feature-flag';
import {FileModule} from 'kopla_app/file';
import {FooterModule} from 'kopla_app/footer';
import {FormModule} from 'kopla_app/form';
import {HeaderModule} from 'kopla_app/header';
import {HttpModule as KoplaHttpModule} from 'kopla_app/http';
import {InfoModule} from 'kopla_app/info';
import {InputFieldModule} from 'kopla_app/input-field';
import {IpModule} from 'kopla_app/ip';
import {IproScvModule} from 'kopla_app/ipro-scv';
import {KoplaCoreCordovaModule} from 'kopla_app/kopla-core-cordova.module';
import {LiabilityDisclaimerModule} from 'kopla_app/liability-disclaimer';
import {LogarithmicPercentageFormatterModule} from 'kopla_app/logarithmic-percentage-formatter';
import {MailModule} from 'kopla_app/mail';
import {MenuModule} from 'kopla_app/menu';
import {NavbarModule} from 'kopla_app/navbar';
import {NotificationModule} from 'kopla_app/notification';
import {NumberModule} from 'kopla_app/number';
import {OverlayModule} from 'kopla_app/overlay';
import {PanelModule} from 'kopla_app/panel';
import {PdfModule} from 'kopla_app/pdf';
import {ProgressCircularModule} from 'kopla_app/progress-circular';
import {RadioGroupModule} from 'kopla_app/radio';
import {RelativeCalculatorModule} from 'kopla_app/relative-calculator';
import {ResourceResolverModule} from 'kopla_app/resource-resolver';
import {ShopModule} from 'kopla_app/shop';
import {StepperModule} from 'kopla_app/stepper';
import {StorageModule} from 'kopla_app/storage';
import {SvgModule} from 'kopla_app/svg';
import {ToolbarModule} from 'kopla_app/toolbar';
import {PopoverModule} from 'ngx-bootstrap/popover';
import {AppBootstrapComponent} from './app-bootstrap.component';
import {AppComponent} from './app.component';
import {applicationConfig} from './app.config';
import {appRoutes} from './app.routing';
import {DayOverviewComponent} from "./day-overview/day-overview.component";
import {GerichtCardComponent} from "./shared/gericht-card/gericht-card.component";
import {HomeComponent} from './home/home.component';
import {ErrorComponent} from './error/error.component';
import {HeaderMenuComponent} from './header-menu/header-menu.component';
import {translateFactory} from './shared/translate.loader';
import {TranslationResolver} from './shared/translation.resolver';
import {ToolbarComponent} from './toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    AppBootstrapComponent,
    ToolbarComponent,
    HeaderMenuComponent,
    ErrorComponent,
    HomeComponent,
    DayOverviewComponent,
    GerichtCardComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    KoplaCoreCordovaModule.forRoot(environment, applicationConfig),
    SvgModule.forRoot(),
    RouterModule.forRoot(appRoutes, {useHash: environment.useHash}),
    PopoverModule.forRoot(),
    FeatureFlagModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: NgTranslateLoader,
        useFactory: translateFactory,
        deps: [HttpClient, CordovaFileService]
      }
    }),
    NavbarModule.forRoot(),
    NotificationModule.forRoot(),
    StepperModule.forRoot(),
    ToolbarModule.forRoot(),
    FooterModule.forRoot(),
    HeaderModule.forRoot(),
    MenuModule.forRoot(),
    ContactFormModule.forRoot(),
    InfoModule.forRoot(),
    InputFieldModule.forRoot(),
    CardModule.forRoot(),
    CheckboxModule.forRoot(),
    CardWrapperModule.forRoot(),
    RadioGroupModule.forRoot(),
    LiabilityDisclaimerModule.forRoot(),
    ProgressCircularModule.forRoot(),
    PanelModule.forRoot(),
    ResourceResolverModule,
    CordovaModule,
    FileModule,
    FormModule,
    PdfModule,
    DeviceInfoModule,
    ShopModule,
    MailModule,
    OverlayModule,
    DropdownModule,
    KoplaHttpModule,
    StorageModule,
    IproScvModule,
    NumberModule,
    RelativeCalculatorModule,
    LogarithmicPercentageFormatterModule,
    CookiePolicyModule,
    AboutModule,
    IpModule,

    ButtonModule.forChild(),
    DialogModule.forRoot()
  ],
  bootstrap: [AppBootstrapComponent],
  providers: [TranslationResolver],
  entryComponents: []
})
export class AppModule {}
