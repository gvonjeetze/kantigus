import {ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {StepperConfig, StepperStrings} from 'kopla_app/stepper';

@Component({
  selector: 'ki-toolbar',
  templateUrl: './toolbar.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {
  public stepperConfig: StepperConfig = {
    steps: []
  };

  public stepperStrings: StepperStrings = {
    step: '',
    of: ''
  };

  constructor(private translateService: TranslateService) {}

  ngOnInit(): void {
    this.translateService.onLangChange.subscribe(() => {
      this.initStepper();
    });
  }

  private initStepper() {
    this.stepperConfig.steps[0] = {
      title: this.translateService.instant('ki.app.STEP_CONFIGURATOR'),
      url: '/configurator'
    };
    this.stepperStrings = {
      step: this.translateService.instant('kopla.stepper.STEP'),
      of: this.translateService.instant('kopla.stepper.OF')
    };
  }

  trackStepClick() {
  }
}
