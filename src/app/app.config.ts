import {TranslateService as NgxTranslateService} from '@ngx-translate/core';
import {ApplicationConfig} from 'kopla_app/application-config';
import {TranslateService} from 'kopla_app/translate';

export const applicationConfig: ApplicationConfig = {
  appName: 'ki',
  availableLocales: ['en', 'de'],
  defaultLocale: 'en',
  defaultCountry: 'DE',
  translateServiceProvider: {provide: TranslateService, useClass: NgxTranslateService}
};
