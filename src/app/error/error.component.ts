import {ChangeDetectionStrategy, Component, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LiabilityDisclaimerService} from 'kopla_app/liability-disclaimer';
import {NotificationService} from 'kopla_app/notification';

@Component({
  selector: 'ki-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent {
  constructor(
    private notificationService: NotificationService,
    private translate: TranslateService,
    private router: Router,
    private liabilityDisclaimerService: LiabilityDisclaimerService
  ) {}

  onClick() {
    this.router.navigate(['/']).then(() => {
      this.notificationService.showNotification({
        type: 'info',
        label: this.translate.instant('kopla.common.CONFIGURATION_RESET_CONFIRMATION')
      });
    });
    this.liabilityDisclaimerService.reset();
  }
}
