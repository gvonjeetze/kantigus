import {ChangeDetectionStrategy, Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AboutComponent} from 'kopla_app/about';
import {UnitSystem, UnitSystemResolver} from 'kopla_app/g11n';
import {LiabilityDisclaimerService} from 'kopla_app/liability-disclaimer';
import {NotificationService} from 'kopla_app/notification';
import {
  KOPLA_APP_VERSION,
  KOPLA_DOMAIN_VERSION,
  KI_APP_VERSION,
  KI_DOMAIN_VERSION
} from '../../version';

@Component({
  selector: 'ki-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderMenuComponent implements OnInit {
  @ViewChild(AboutComponent)
  private aboutOverlay: AboutComponent;

  @Input()
  homepageUrl: string = '/';
  @Input()
  imprintUrl: string = '/';
  @Input()
  dataPrivacyUrl: string = '/';

  @Input()
  imprintLabel: string = '_imprint';
  @Input()
  dataPrivacyLabel: string = '_dataPrivacy';

  public unitSystem: UnitSystem;
  public versions = [
    {name: 'ki_expert', number: KI_APP_VERSION},
    {name: 'ki_domain', number: KI_DOMAIN_VERSION},
    {name: 'kopla_app', number: KOPLA_APP_VERSION},
    {name: 'kopla_domain', number: KOPLA_DOMAIN_VERSION}
  ];

  constructor(
    private translate: TranslateService,
    private router: Router,
    private notificationService: NotificationService,
    private unitSystemResolver: UnitSystemResolver,
    private liabilityDisclaimerService: LiabilityDisclaimerService,
  ) {}

  ngOnInit(): void {
    this.initUnitSystemSwitch();
  }

  private initUnitSystemSwitch() {
    this.unitSystemResolver.unitSystem$.subscribe(unitSystem => (this.unitSystem = unitSystem));
  }

  /**
   * Decides what configuration function to call based on the triggered configuration management component event.
   */
  handleConfigurationActions(event: any): void {
    if (!event || !event.confEventName) return;
    switch (event.confEventName) {
      case 'not-supported': // loading and saving config files is not supported e.g. any browser on iOS
        this.notifyError(this.translate.instant('kopla.common.UNAVAILABLE_IOS_FEATURE'));
        break;
      case 'save-network-error': // echo service used for config file download not reachable
        this.notifyError(this.translate.instant('kopla.common.CONFIGURATION_NETWORK_ERROR'));
        break;
      case 'load-error': // loading of configFile was not possible
        this.notifyError(this.translate.instant('kopla.common.CONFIGURATION_LOADING_ERROR'));
        break;
      case 'load-error-extension': // provided configFile had wrong extension
        this.notifyError(this.translate.instant('kopla.common.CONFIGURATION_LOADING_EXTENSION_ERROR'));
        break;
      case 'load-error-app-name': // provided configFile had wrong extension
        this.notifyError(this.translate.instant('kopla.common.CONFIGURATION_LOADING_APP_NAME_ERROR'));
        break;
      case 'load-ok': // config file loaded. content is in payload of event
        this.notifyInfo(this.translate.instant('kopla.common.CONFIGURATION_LOADING_SUCCESS'));
        break;
    }
  }

  get loadNewConfig() {
    return () => {
      this.trackNewConfigClick();
      this.router.navigate(['/']).then(() => {
        this.notifyInfo(this.translate.instant('kopla.common.CONFIGURATION_RESET_CONFIRMATION'));
      });
      this.liabilityDisclaimerService.reset();
    };
  }


  get openAboutPage() {
    return () => {
      this.aboutOverlay.openAboutPage();
    };
  }

  get openImprintPage() {
    return () => {
      window.open(this.imprintUrl, '_blank');
    };
  }

  get openPrivacyPage() {
    return () => {
      window.open(this.dataPrivacyUrl, '_blank');
    };
  }

  private notifyError(label: string, clickHandler?: any, link?: string) {
    this.notificationService.showNotification({
      type: 'error',
      label: label,
      clickHandler: clickHandler,
      linkLabel: link
    });
  }

  private notifyInfo(label: string, clickHandler?: any, link?: string, timeout?: boolean) {
    this.notificationService.showToast({
      type: 'info',
      label: label,
      clickHandler: clickHandler,
      linkLabel: link,
      timeout: timeout
    });
  }

  /****************************************************************************************
   * Tracking
   */

  trackNewConfigClick() {
  }

  trackMetricClick() {
  }

  trackImperialClick() {
  }

  trackMenuIconClick() {
  }

  trackSaveButtonClick() {
  }

  trackLoadButtonClick() {
  }

  trackShoppingBasketButtonClick() {
  }

  trackViewClick(viewId: string) {
    switch (viewId) {
      case 'configuration':
        break;
      case 'unitsystemswitch':
        break;
    }
  }
}
