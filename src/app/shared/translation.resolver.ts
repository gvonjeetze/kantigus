import {Injectable} from '@angular/core';
import {Resolve} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {ApplicationConfig} from 'kopla_app/application-config';
import {LanguageResolver} from 'kopla_app/g11n';
import {Observable} from 'rxjs';
import {switchMap} from 'rxjs/operators';

@Injectable()
export class TranslationResolver implements Resolve<any> {
  constructor(
    private translate: TranslateService,
    private languageResolver: LanguageResolver,
    private applicationConfig: ApplicationConfig
  ) {}

  resolve(): Observable<any> {
    return this.languageResolver.getIETFLanguageTag().pipe(
      switchMap(language => {
        const lang = this.isLanguageAvailable(language) ? language : this.applicationConfig.defaultLocale;
        return this.translate.use(lang);
      })
    );
  }

  private isLanguageAvailable(lang: string) {
    return this.applicationConfig.availableLocales.indexOf(lang) >= 0;
  }
}
