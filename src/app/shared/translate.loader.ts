import {HttpClient} from '@angular/common/http';
import {TranslateLoader as NgTranslateLoader} from '@ngx-translate/core';
import {CordovaFileService, CordovaService} from 'kopla_app/cordova';
import {from, Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {KI_APP_VERSION} from '../../version';

export class TranslateLoader implements NgTranslateLoader {
  constructor(
    private http: HttpClient,
    private cordovaFileService: CordovaFileService,
    private prefix: string,
    private suffix: string,
    private version: string
  ) {}

  /**
   * Gets the translations from the server or the file system if running inside cordova app
   * @param lang
   */
  getTranslation(lang: string): Observable<any> {
    if (CordovaService.appRunsOnCordovaPlatform(window)) {
      const fileDirectory = `${window.cordova.file.applicationDirectory}www${this.prefix}/${this.version}/`;
      const fileName = `${lang}.${this.version}${this.suffix}`;
      return from(this.cordovaFileService.getFileData(fileDirectory, fileName, false)).pipe(
        map(data => JSON.parse(data))
      );
    } else {
      return this.http.get(`${this.prefix}/${lang}.${this.version}${this.suffix}`);
    }
  }
}

export function translateFactory(http: HttpClient, cordovaFileService: CordovaFileService) {
  return new TranslateLoader(http, cordovaFileService, '/i18n', '.json', KI_APP_VERSION);
}
