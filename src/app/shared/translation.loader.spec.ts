import {HttpClient} from '@angular/common/http';
import {inject, TestBed} from '@angular/core/testing';
import {CordovaFileService} from 'kopla_app/cordova';
import {of} from 'rxjs';
import {KI_APP_VERSION} from '../../version';
import {translateFactory, TranslateLoader} from './translate.loader';

describe('TranslateLoader', () => {
  class CordovaFileServiceMock {
    getFileData() {}
  }

  class HttpMock {
    get() {}
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        {provide: CordovaFileService, useClass: CordovaFileServiceMock},
        {provide: HttpClient, useClass: HttpMock}
      ]
    });
  });

  let translateLoader: TranslateLoader;
  beforeEach(inject([CordovaFileService, HttpClient], (_cordovaFileService: CordovaFileService, _http: HttpClient) => {
    translateLoader = translateFactory(_http, _cordovaFileService);
  }));

  describe('getTranslation', () => {
    it('should request by http', done => {
      const mockSpy = spyOn(HttpMock.prototype, 'get').and.returnValue(of({json: () => '{}'}));
      translateLoader.getTranslation('en').subscribe(() => {
        expect(mockSpy).toHaveBeenCalledWith(`/i18n/en.${KI_APP_VERSION}.json`);
        done();
      });
    });

    it('should request file service on cordova', done => {
      (<any>window.cordova) = {file: {applicationDirectory: ''}};
      const mockSpy = spyOn(CordovaFileServiceMock.prototype, 'getFileData').and.returnValue(Promise.resolve('{}'));
      translateLoader.getTranslation('en').subscribe(() => {
        expect(mockSpy).toHaveBeenCalled();
        delete window.cordova;
        done();
      });
    });
  });
});
