import {ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {Allergene, Naehrwert} from '../../day-overview/day-overview.component';

export interface Gericht {
  id: number;
  name: string;
  teaser: string;
  allergene?: Allergene[];
  bild?: string;
  naehrwerte?: Naehrwert[];
}

@Component({
  selector: 'ki-gericht-card',
  templateUrl: './gericht-card.component.html',
  styleUrls: ['./gericht-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GerichtCardComponent implements OnInit {
  @Input() gericht: Gericht;

  public imagePath: string = '../../images/default_essen.jpg';

  constructor() {}

  ngOnInit(): void {
    if (this.gericht.bild) {
      this.imagePath = '../../images/' + this.gericht.bild;
    }
  }
}
