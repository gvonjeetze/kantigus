import {inject, TestBed} from '@angular/core/testing';
import {TranslateService} from '@ngx-translate/core';
import {ApplicationConfig} from 'kopla_app/application-config/application-config';
import {LanguageResolver} from 'kopla_app/g11n/language-resolver.service';
import {of} from 'rxjs';
import {TranslationResolver} from './translation.resolver';

describe('TranslationResolver', () => {
  class ApplicationConfigMock {
    defaultLocale = 'en';
    availableLocales = ['de', 'en', 'pl'];
  }

  class TranslateServiceMock {
    use = (key: string) => of(key);
  }

  class LanguageResolverMock {
    getIETFLanguageTag() {
      return of('pl');
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        TranslationResolver,
        {provide: ApplicationConfig, useClass: ApplicationConfigMock},
        {provide: LanguageResolver, useClass: LanguageResolverMock},
        {provide: TranslateService, useClass: TranslateServiceMock}
      ]
    });
  });

  let resolver: TranslationResolver;
  beforeEach(inject([TranslationResolver], (_ref: TranslationResolver) => {
    resolver = _ref;
  }));

  describe('resolve', () => {
    it('should return pl', done => {
      resolver.resolve().subscribe(culture => {
        expect(culture).toBe('pl');
        done();
      });
    });

    it('should return en', done => {
      spyOn(LanguageResolverMock.prototype, 'getIETFLanguageTag').and.callFake(() => of(true));
      resolver.resolve().subscribe(culture => {
        expect(culture).toBe('en');
        done();
      });
    });
  });
});
