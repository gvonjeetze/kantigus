import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {delay, filter, first, map} from 'rxjs/operators';

/**
 * The `AppBootstrapComponent` has the responsibility to
 * bootstrap the whole application as wrapping element.
 *
 * This allows us to use an AppResolver before any other Component is initialized.
 *
 * If we would use the AppComponent directly, its and its template-nested child components ngOnInit
 * methods would be called before the AppResolver.
 */
@Component({
  selector: 'ki-app-bootstrap',
  template: '<router-outlet></router-outlet>',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppBootstrapComponent implements OnInit {
  /**
   * Removes splash from html
   */
  private static removeSplashScreen() {
    const splashElem: Element = document.querySelector('#ki_splash');
    if (splashElem) {
      splashElem.setAttribute('style', 'display:none;');
    }

    if ('splashscreen' in navigator) {
      (<any>navigator)['splashscreen'].hide();
    }
  }

  constructor(private router: Router) {}

  /**
   * Removes splash after first navigation ends occurs
   */
  ngOnInit(): void {
    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd),
        map(e => e as NavigationEnd),
        first(),
        delay(100)
      )
      .subscribe(() => AppBootstrapComponent.removeSplashScreen());
  }
}
