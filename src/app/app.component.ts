import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Event, NavigationEnd, NavigationError, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {DomainResolver, LocaleResolver} from 'kopla_app/g11n';
import {LiabilityDisclaimerService} from 'kopla_app/liability-disclaimer';
import {NotificationService} from 'kopla_app/notification';
import {ShopSessionHelperService} from 'kopla_app/shop';
import {switchMap, tap} from 'rxjs/operators';

@Component({
  selector: 'ki-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  /**
   * Url for the imprint pop up
   * Base url is http://www.igus.de/Imprint
   * Several parameters are needed to open the imprint page as:
   * Parameter `POP=yes` allows window to open in pop up
   * Parameters of culture with `c={{country}}&l={{language}}`
   */
  imprintUrl: string = 'http://www.igus.de/Imprint';

  /**
   * Url fot the data privacy page
   * Needs to be formatted as
   * http://www.igus.de/r05/staticContent/dse/dse_{{COUNTRY}}{{language}}.html
   */
  dataPrivacyUrl: string = 'http://www.igus.de/r05/staticContent/dse/dse_';

  /**
   * Url to the igus' homepage
   * Has to be localized
   */
  homepageUrl: string;

  constructor(
    private router: Router,
    public notificationService: NotificationService,
    private elementRef: ElementRef,
    private liabilityDisclaimerService: LiabilityDisclaimerService,
    private sessionHelperService: ShopSessionHelperService,
    private translate: TranslateService,
    private title: Title,
    private changeDetectorRef: ChangeDetectorRef,
    private localeResolver: LocaleResolver,
    private domainResolver: DomainResolver,
    //@ts-ignore
  ) {}

  ngOnInit() {
    this.setTitle();
    this.initRouter();
    this.initSessionHelperService();

    this.localeResolver
      .getLocale()
      .pipe(
        switchMap(({language, countryCode}) => {
          this.setCultureInMetaTags(language, countryCode);
          return this.setLocalizedLinks(language, countryCode);
        })
      )
      .subscribe();
  }

  /****************************************************************************************
   * Initializing global services
   */

  setCultureInMetaTags(language: string, country: string) {
    const metaTags = document.getElementsByTagName('meta');
    Array.prototype.forEach.call(metaTags, (metaTag: HTMLMetaElement) => {
      if (metaTag.getAttribute('name') === 'DCSext.M_Country') metaTag.setAttribute('content', country);
      if (metaTag.getAttribute('name') === 'DCSext.M_Language') metaTag.setAttribute('content', language);
    });
  }

  private initRouter() {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.notificationService.resetNotifications();
        this.notificationService.resetToasts();
        this.scrollToTop();
      }
      if (event instanceof NavigationError) {
        document.location.assign('/error');
      }
    });
  }

  private setLocalizedLinks(language: string, countryCode: string) {
    return this.domainResolver.getDomainForCustomLocale({language, countryCode}).pipe(
      tap(domain => {
        this.homepageUrl = `https://${domain}`;
        this.imprintUrl += `?POP=yes&c=${countryCode}&l=${language}`;
        this.dataPrivacyUrl += `${countryCode}${language}.htm`;
        this.changeDetectorRef.markForCheck();
      })
    );
  }

  private initSessionHelperService() {
    this.sessionHelperService.sessionRemoved.subscribe(() => {
      this.liabilityDisclaimerService.reset();
    });
  }

  private setTitle() {
    this.translate.get('ki.app.TITLE').subscribe(title => {
      this.title.setTitle(title);
    });
  }

  public scrollToTop() {
    this.elementRef.nativeElement.scrollIntoView();
  }
}
