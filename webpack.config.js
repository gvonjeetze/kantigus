const process = require('process');
const {config} = require('kopla_app/tools/webpack.helper');

const kiJson = require('./package.json');
// Uncomment to provide domain version to application
//const kiDomainJson = require('PACKAGE_DOMAIN_NAME_LC/package.json');
const koplaJson = require('kopla_app/package.json');
const koplaDomainJson = require('kopla_domain/package.json');

let isProduction = false;
switch (process.env.NODE_ENV) {
  case 'prod':
  case 'production':
  case 'staging':
  case 'integration':
  case 'cordova':
  case 'cordova-production':
    isProduction = true;
    break;
}

const replaceVersions = [
  {search: 'KI_APP_PLACEHOLDER', replace: kiJson.version},
  // Uncomment to provide domain version to application
  //{search: 'KI_DOMAIN_PLACEHOLDER', replace: kiDomainJson.version},
  {search: 'KOPLA_APP_PLACEHOLDER', replace: koplaJson.version},
  {search: 'KOPLA_DOMAIN_PLACEHOLDER', replace: koplaDomainJson.version}
];

const copyRules = [
  {from: 'src/images', to: 'images'},
  {from: 'src', to: '.', test: /favicon\.ico/}
];

module.exports = config({
  copyRules,
  isProduction,
  replaceVersions
});
