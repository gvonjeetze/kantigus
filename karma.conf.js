module.exports = function(config) {
  require('kopla_app/tools/webpack.helper').karma(config, {
    scssDir: './src/assets/scss',
    thresholds: {
      global: {
        statements: 65, // please set global coverage percentage here
        branches: 46,
        functions: 33,
        lines: 65
      }
    }
  });
};
