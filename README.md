# Kantigus

# Building

## Prerequisite Software

- [igus kopla local environment](https://igusdev.atlassian.net/wiki/display/KINF/Lokale+Entwicklungsumgebung)

## Installing NPM Modules

```shell
$ npm install
```

## Build commands

- `$ npm run start:web` build and serve the app under localhost:4200
- `$ npm run start:web:aot` build and serve the app+aot under localhost:4200
- `$ npm run start` alias to `npm run start:web`
- `$ npm run start:aot` alias to `npm run start:web:aot`
- `$ npm run watch` build and serve the app under localhost:4200; additionally: watch
- `$ npm run watch:aot` build and serve the app+aot under localhost:4200; additionally: watch
- `$ [NODE_ENV=env &&] npm run build` build with environment (default empty)
- `$ [NODE_ENV=env &&] npm run build:aot` build aot with environment (default empty)
- `$ npm run build:cordova` build for cordova
- `$ npm run build:cordova:aot` build aot for cordova
- `$ npm run lint` runs lint
- `$ npm run analyze` runs bundle analyze
- `$ npm run analyze:aot` runs aot bundle analyze

Arguments

- env {development|integration|staging|production}

Most of the commands default to --platform=local --env=development

# Testing

To run the tests

- `$ npm run test` runs all unit and integration tests
- `$ npm run e2e` runs all e2e tests

For more informations, read [igus kopla best practices - testing](https://igusdev.atlassian.net/wiki/display/KOPLA10/Best+practices#Bestpractices-Testing)

# Distributing

Read about [branching & deployments](https://igusdev.atlassian.net/wiki/pages/viewpage.action?pageId=8224800)
Read about [linking and using npm packages](https://igusdev.atlassian.net/wiki/display/KOPLA10/NPM+Verlinkung)

To distribute for web, run `$ gulp web` and use the build in folder `dist/web`.

# Developing

## Code Style

- [igus kopla code style](https://igusdev.atlassian.net/wiki/display/KOPLA10/Allgemeine+Konventionen#space-menu-link-content)

## Folder structure

- `cordova`: Distribution build for cordova platform
- `dist`: Distribution build for web
- `e2e`: Angular e2e config and spec files
- `i18n`: Message translation files for translators
- `spec`: Angular e2e and unit test config files
- `src`: Source code

## Debugging

### Debug the tests

- [igus kopla best practices - debugging tests](https://igusdev.atlassian.net/wiki/display/KOPLA10/Best+practices#Bestpractices-Debugging)

## Creating translation files

- [I18N](https://igusdev.atlassian.net/wiki/display/KOPLA10/I18n)
- [Translation process](https://igusdev.atlassian.net/wiki/display/KOPLA10/Translation+process)

- `$ gulp i18n:genRepos` bundles all i18n files for translation in folder `i18n`.
- `$ gulp i18n:parseRepos` de-bundles translation bundles back to src folder.
- `$ gulp i18n:getS3TranslationsAndParseRepos` get translation from S3 and de-bundles translations back to src folder.

- `$ npm run i18n:download` download all i18n files from s3 (fromTranslation)
- `$ npm run i18n:parse` parse the translations in i18n root folder
- `$ npm run i18n:download:parse` combine download and parse translations
- `$ npm run i18n:generate` generates translations files that will be moved to s3 (toTranslation)
