import {browser, by, element, ElementArrayFinder, ElementFinder} from 'protractor';

export class CommonPage {
  static navigateTo(page: string, features: string[] = [], lang: string = 'en') {
    let featuresUrl = '';
    if (features.length > 0) {
      for (const feature of features) {
        featuresUrl += `;FF-${feature}=1`;
      }
    }
    // Waiting is enabled and disabled to be sure the page changed properly (another solution is to sleep browser)
    browser.waitForAngularEnabled(true);
    return browser.get(`/${page}${featuresUrl}?c=${lang.toUpperCase()}&l=${lang}`).then(() => {
      browser.waitForAngularEnabled(false);
    });
  }

  static getHeaderTitle() {
    return element(by.css('.kopla-header__title')).getText();
  }

  static getPageTitle() {
    return element(by.css('.kopla-page__title')).getText();
  }

  static getActiveStepText() {
    return element(by.css('.kopla-stepper__step--active .kopla-stepper__label')).getText();
  }

  static hasClass(elem: ElementFinder, cls: string) {
    browser.waitForAngularEnabled(false);
    return elem.getAttribute('class').then(function(classes) {
      return classes.split(' ').indexOf(cls) !== -1;
    });
  }

  static radioItemSelected(elem: ElementFinder) {
    return CommonPage.hasClass(elem, 'kopla-radio-item--selected');
  }

  static getSelectedItem(elem: ElementArrayFinder) {
    return elem
      .filter(el => {
        return el.isSelected();
      })
      .first();
  }
}
