Error.stackTraceLimit = Infinity;

require('./polyfills');

const TestBed = require('@angular/core/testing').TestBed;
const testing = require('@angular/platform-browser-dynamic/testing');

TestBed.initTestEnvironment(testing.BrowserDynamicTestingModule, testing.platformBrowserDynamicTesting());

// Needed for full coverage (for files that doesn't have spec files)
const mainContext = require.context('../src/app', true, /^(?!.*\.(spec|int-spec|http-spec|d)\.ts$).*\.ts$/);
mainContext.keys().map(mainContext);
